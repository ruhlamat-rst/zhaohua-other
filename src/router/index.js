import Vue from 'vue';
import Router from 'vue-router';
import ShiftSchedule from '../pages/ShiftSchedule';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      component: ShiftSchedule,
    },
  ],
});

export default router;
