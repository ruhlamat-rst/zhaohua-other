import Service from '../plugins/axios';

const reqConfig = { timeout: 45000 };

export default {
  getService(elementName, customerId, userId, queryParams) {
    try {
      const query = queryParams || '';
      const url = `/server/${elementName}?customerid=${customerId}&userid=${userId}${query}`;
      return Service.get(url, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },
};
