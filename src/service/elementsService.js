import Service from '../plugins/axios';

const reqConfig = { timeout: 45000 };

export default {
  getElement(elementName, customerId, userId, queryParams) {
    try {
      const query = queryParams || '';
      const url = `/server/elements/${elementName}/records?customerid=${customerId}&userid=${userId}${query}`;
      return Service.get(url, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },

  postElement(elementName, customerId, userId, postData) {
    try {
      const url = `/server/elements/${elementName}/records?customerid=${customerId}&userid=${userId}`;
      return Service.post(url, postData, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },

  postElementMultiple(elementName, customerId, userId, postData) {
    try {
      const url = `/server/elements/${elementName}/createbulkrecords/v2?customerid=${customerId}&userid=${userId}`;
      return Service.post(url, postData, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },

  putElement(elementName, _id, customerId, userId, postData) {
    try {
      const url = `/server/elements/${elementName}/records/${_id}?customerid=${customerId}&userid=${userId}`;
      return Service.put(url, postData, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },

  deleteElement(elementName, _id, customerId, userId, postData) {
    try {
      const url = `/server/elements/${elementName}/records/${_id}?customerid=${customerId}&userid=${userId}`;
      return Service.delete(url, postData, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },

  getElementNoRecord(elementName, customerId, userId, queryParams) {
    try {
      const query = queryParams || '';
      const url = `/server/elements/${elementName}?customerid=${customerId}&userid=${userId}${query}`;
      return Service.get(url, reqConfig);
    } catch (e) {
      throw new Error(e);
    }
  },
};
