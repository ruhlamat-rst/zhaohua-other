import { set } from '../../utils/helper';
// import ServiceFactory from '../service/ServiceFactory';
// const { CommonService } = ServiceFactory;
// let timeout = null;
export default {
  state: {
    isLoading: false,
    isShowKeyboard: false,
    keyboardValue: '',
    servertime: '',
    isShowSetting: localStorage.getItem('customerId') && localStorage.getItem('userId') ? false : true,
    customerId: localStorage.getItem('customerId') || '',
    userId: localStorage.getItem('userId') || '',
    machineid: localStorage.getItem('machineid') || '',
  },
  mutations: {
    SET_lOADING: set('isLoading'),
    SET_KEYBOARD: set('isShowKeyboard'),
    SET_KEYBOARD_VALUE: set('keyboardValue'),
    SET_SERVERTIME: set('servertime'),
    SET_SETTING: set('isShowSetting'),
    SET_CUSTOMERID: set('customerId'),
    SET_USERID: set('userId'),
    SET_MACHINEID: set('machineid'),
  },
  actions: {
    // GET_SERVERTIME:(({state}) => {
    //   console.log(state, 'state')
    //   const { customerId, userId } = state;
    // }) 
    // CONFIRM_BASKET: async ({ commit, rootState }, barcode) => {
    //   const { serverIp, customerId, userId } = rootState.helper;
    //   try {
    //     const postData = {
    //       all: true,
    //       basketnumberval: barcode,
    //     };
    //     const { data } = await ReportService.getReport(
    //       serverIp,
    //       'basketrecord',
    //       customerId,
    //       userId,
    //       postData,
    //       '&all=true',
    //     );
    //     let { reportData } = data;
    //     reportData = JSON.parse(reportData);
    //     if (reportData.reportData.length) {
    //       const items = reportData.reportData.filter((d) => {
    //         d.quantityActual = '';
    //         d.isLoading = false;
    //         return d.basketnumber === barcode;
    //       });
    //       console.log(items);
    //       commit('ADD_ITEMS', items);
    //       if (items.length === 0) {
    //         commit('SET_ERROR', i18n.t('basket.errors.invalidInput', { barcode }));
    //       }
    //     } else {
    //       commit('SET_ERROR', i18n.t('basket.errors.invalidInput', { barcode }));
    //     }
    //   } catch (e) {
    //     commit('SET_ERROR', e.message);
    //   }
    // },
  },
  getters: {
    // enableButtons: ({ items }) => (items && items.length > 0 && items[0].quantityActual > 0),
  },
};
