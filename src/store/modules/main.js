import ServiceFactory from '../../service/ServiceFactory';
const { ElementsService } = ServiceFactory;
export default {
  state: {
  },
  mutations: {
  },
  actions: {
    GET_ELEMENT:async ({rootState, commit}, payload) => {
      // console.log(commit, 'state');
      const { customerId, userId } = rootState.helper;
      const { element, query } = payload;
      commit('helper/SET_lOADING', true, {root: true});
      const { data } = await ElementsService.getElement(
        element,
        customerId,
        userId,
        query || ''
      );
      const { results } = data;
      commit('helper/SET_lOADING', false, {root: true});
      if (results.length > 0) {
        return results;
      }
      return false;
    },
  },
};
