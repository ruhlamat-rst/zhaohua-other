module.exports = {
  "publicPath": "./",
  "devServer": {
    "proxy": {
      // '/socket.io': {
      //   target: 'http://192.168.1.200:10190/socket.io',
      //   changeOrigin: false
      // },
      "/server": {
        "target": "http://192.168.1.220/server",
        "changeOrigin": true,
        "pathRewrite": {
          "^/server": ""
        }
      }
    }
  },
  transpileDependencies: [
    "vuetify"
  ]
}